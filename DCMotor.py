class DCMotor:
    def __init__(self, pin1, pin2, enable1_pin, enable2_pin, stby_pin):
        self.pin1=pin1
        self.pin2=pin2
        self.enable1_pin=enable1_pin
        self.enable2_pin=enable2_pin
        self.stby_pin=stby_pin

    def forward(self, speed):
        self.speed = speed
        self.stby_pin.value(1)
        self.enable1_pin.duty(self.speed)
        self.enable2_pin.duty(self.speed)
        self.pin1.value(1)
        self.pin2.value(1)

    def backwards(self, speed):
        self.speed = speed
        self.stby_pin.value(1)
        self.enable1_pin.duty(self.speed)
        self.enable2_pin.duty(self.speed)
        self.pin1.value(0)
        self.pin2.value(0)

    def stop(self):
        self.enable_pin.duty(0)
        self.pin1.value(0)
        self.pin2.value(0)
