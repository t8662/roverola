import machine
import utime
''
#Front to back
pin1 = machine.Pin(17)
servo1 = machine.PWM(pin1, freq=50)
#max(front) 110 min(back) 80
''
''
#Grip
pin2 = machine.Pin(16)
servo2 = machine.PWM(pin2, freq=50)
#max(close)- 31 minimum(open)- 130
''
''
#Turn
pin3 = machine.Pin(23)
servo3 = machine.PWM(pin3, freq=50)
#max(left)- 130 minimum(right)- 20
''
''
#Up and down
pin4 = machine.Pin(18)
servo4 = machine.PWM(pin4, freq=50)
#max(up)- 100 minimum(down)- 35
''
