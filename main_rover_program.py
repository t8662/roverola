try:
    import usocket as socket
except:
    import socket

from machine import Pin, PWM
import network
from utime import sleep
from ultrsnc import ultrsnc
from hcsr04 import HCSR04
from DCMotor import DCMotor
from servo_cont import servo_control
import _thread

KillThread = 1

# Defining the left and right motor pins.

frequency1 = 15000

# Right side

R_stby = Pin(14, Pin.OUT)
R_pha_1 = Pin(27, Pin.OUT)
R_pha_2 = Pin(13, Pin.OUT)
R_en_1 = PWM(Pin(26), frequency1)
R_en_2 = PWM(Pin(12), frequency1)

# Left side

L_stby = Pin(4, Pin.OUT)
L_pha_1 = Pin(17, Pin.OUT)
L_pha_2 = Pin(0, Pin.OUT)
L_en_1 = PWM(Pin(16), frequency1)
L_en_2 = PWM(Pin(15), frequency1)

# Create two objects for the two sides

right = DCMotor(R_pha_1, R_pha_2, R_en_1, R_en_2, R_stby)
left = DCMotor(L_pha_1, L_pha_2, L_en_1, L_en_2, L_stby)

# Ultrasonic sensor setup

L_sensor = HCSR04(trigger_pin = 18, echo_pin = 5, echo_timeout_us = 20000)
R_sensor = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 20000)

ult_sensors = ultrsnc(L_sensor, R_sensor)

# Calls upon distance and speed calculation function, applies to the motors then sleeps




# Servo stuff

servopin1 = Pin(23)
servo1 = PWM(servopin1, freq=50)
servopin3 = Pin(19)
servo3 = PWM(servopin3, freq=50)
servopin2 = Pin(22)
servo2 = PWM(servopin2, freq=50)
servopin4 = Pin(21)
servo4 = PWM(servopin4, freq=50)

servos = servo_control(servo1, servo2, servo3, servo4)


# Setup AP. Remember to change essid.
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'Team9', password = 'teamnine')
station.config(authmode=3)

while station.isconnected() == False:
    pass

print('Connection successful')
print(station.ifconfig())

def auto(L_speed, R_speed):
    right.forward(R_speed)
    left.forward(L_speed)

def auto_pilot():
    while True:
        L_speed, R_speed = ult_sensors.dist_calc()
        auto(L_speed, R_speed)
        sleep(0.1)
        if KillThread == 1:
            _thread.exit()
            

def web_page(request):
    global KillThread
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state="Going Forward"
        right.forward(1023)
        left.forward(1023)
    if request.find('/?left') > 0:
        motor_state="Going Left"
        right.forward(1023)
        left.backwards(1023)
    if request.find('/?right') > 0:
        motor_state="Going Right"
        right.backwards(1023)
        left.forward(1023)
    if request.find('/?backwards') > 0:
        motor_state="Going Back"
        right.backwards(1023)
        left.backwards(1023)
    if request.find('/?stop') > 0:
        motor_state="Stopped"
        right.stop()
        left.stop()
    if request.find('/?Forward') > 0:
        servos.servo2_adjust(5)
    if request.find('/?Back') > 0:
        servos.servo2_adjust(-5)
    if request.find('/?Up') > 0:
        servos.servo4_adjust(5)
    if request.find('/?Down') > 0:
        servos.servo4_adjust(-5)
    if request.find('/?Turn_left') > 0:
        servos.servo1_adjust(5)
    if request.find('/?Turn_right') > 0:
        servos.servo1_adjust(-5)
    if request.find('/?Grap') > 0:
        servos.servo3_adjust()
    if request.find('/?Auto') > 0:
        KillThread = 0
        _thread.start_new_thread(auto_pilot, ())
    if request.find('/?Manual') > 0:
        KillThread = 1
        
    
    html = '''<html><head> <title>Rover Web Server</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="data:,"> <style>
    html{font-family: Helvetica; display:inline-block; margin: 0px auto;
    text-align: center;}
    h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}
    .button{display: inline-block; background-color: #e7bd3b; border: none;
    border-radius: 4px; color: white; text-decoration: none;
    font-size: 30px; width:100%} 
    .button2{background-color: #4286f4; width:49%}
    </style></head>
    <body> <h1>Rover Web Server</h1>
    <p>Rover : <strong>Man-cess</strong></p>
    <p><a href='/?forward'><button class="button">Forward</button></a></p>
    <p><a href='/?left'><button class="button button2">LEFT</button></a>
    <a href='/?right'><button class="button button2" >RIGHT</button></a></p>
    <p><a href='/?backwards'><button class="button button">Backwards</button></a></p>
    <p><a href='/?stop'><button class="button button">STOP</button></a></p>
    <p><a href='/?Forward'><button class="button button2">Forward</button></a>
    <a href='/?Up'><button class="button button2" >Up</button></a></p>
    <p><a href='/?Back'><button class="button button2">Back</button></a>
    <a href='/?Down'><button class="button button2" >Down</button></a></p>
    <p><a href='/?Turn_left'><button class="button button2">Turn_left</button></a>
    <a href='/?Turn_right'><button class="button button2" >Turn_right</button></a></p>
    <p><a href='/?Grap'><button class="button button">Grap</button></a></p>
    <p><a href='/?Auto'><button class="button button">Auto</button></a></p>
    <p><a href='/?Manual'><button class="button button">Manual</button></a></p>
    </body></html>'''
    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    response = web_page(request[:20])
    conn.send(response)
    conn.close()